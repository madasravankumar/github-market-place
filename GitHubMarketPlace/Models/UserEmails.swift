//
//  UserEmails.swift
//  GitHubMarketPlace
//
//  Created by Abinavu on 03/04/20
//  Copyright © 2019 Abinavu. All rights reserved.
//

import Foundation


struct UserEmails: Codable {
    var email: String
    var verified: Bool
    var primary: Bool
    var visibility: String
}
