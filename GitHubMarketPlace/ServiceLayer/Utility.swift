//
//  Utility.swift
//  GitHubMarketPlace
//
//  Created by Abinavu on 03/04/20.
//  Copyright © 2019 Abinavu. All rights reserved.
//

import Foundation

class Utility {
    static let shared: Utility = Utility()
    
    private init() { }
    
    var cache: [String: Int] = [String: Int]()
    
}
